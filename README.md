# Reactr demo

## Create and build a "Swift" Runnable

```bash
# create the runnable
subo create runnable add --lang=swift
# build the runnable
subo build hello/
```

## Build the Runnable launcher

```bash
go build -o play
```
